import { Body, Controller, ParseIntPipe, Post, Query, Req } from "@nestjs/common";
import { AppService } from "./app.service";
import {
  ActiveCardDto, BlockMoneyDto,
  CardBalanceDto, ChargeCardDto,
  CreateCustomerDto,
  CreateWalletDto, GetTransactionsDto, InfoByCardNumberDto, InfoByNationalCodeDto,
  InQueryCardDto,
  IssueCardDto, SendOtpDto, SendTransferOtpDto, ShowBlockMoneyDto, UnblockMoneyDto, UpdateCustomerAddressDto,
  WalletBalanceDto, WalletToIbanDto, WalletToWalletDto
} from "./common/dto";
import { SecondPinDto } from "./common/dto/second-pin.dto";
import { PinCodeDto } from "./common/dto/pin-card.dto";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {
  }

  @Post("/createCustomer")
  async createCustomer(
    @Body() createCustomerDto: CreateCustomerDto
  ) {
    return {
      data: await this.appService.createCustomer(createCustomerDto)
    };
  }

  @Post("/createWallet")
  async createWallet(
    @Body() createWalletDto: CreateWalletDto
  ) {
    return {
      data: await this.appService.createWallet(createWalletDto)
    };
  }

  @Post("/issueCard")
  async issueCard(
    @Body() issueCardDto: IssueCardDto
  ) {
    return {
      data: await this.appService.issueCard(issueCardDto)
    };
  }

  @Post("/inQueryCard")
  async inQueryCard(
    @Body() inQueryCardDto: InQueryCardDto
  ) {
    return {
      data: await this.appService.inQueryCard(inQueryCardDto.nationalCode)
    };

  }

  @Post("/cardBalance")
  async cardBalance(
    @Body() cardBalanceDto: CardBalanceDto
  ) {
    return {
      data: await this.appService.cardBalance(cardBalanceDto.cardNumber)
    };
  }

  @Post("/walletBalance")
  async walletBalance(
    @Body() walletBalanceDto: WalletBalanceDto
  ) {
    return {
      data: await this.appService.walletBalance(walletBalanceDto.walletNumber)
    };
  }

  @Post("/sendOtp")
  async sendOtp(
    @Body() sendOtpDto: SendOtpDto
  ) {
    return {
      data: await this.appService.sendOtp(sendOtpDto.cardNumber)
    };
  }

  @Post("/sendTransferOtp")
  async sendTransferOtp(
    @Body() sendTransferOtpDto: SendTransferOtpDto
  ) {
    return {
      data: await this.appService.sendTransferOtp(sendTransferOtpDto)
    };
  }


  @Post("/pinCard/change")
  async changePin(
    @Body() newPinDto: SecondPinDto
  ) {
    return {
      data: await this.appService.changeSecondPin(newPinDto)
    };
  }


  @Post("/firstPin/change")
  async changeFirstPin(
    @Body() newPinDto: PinCodeDto
  ) {
    return {
      data: await this.appService.changeFirstPin(newPinDto)
    };
  }


  @Post("/chargeCard")
  async chargeCard(
    @Body() chargeCardDto: ChargeCardDto
  ) {
    return {
      data: await this.appService.chargeCard(chargeCardDto)
    };
  }

  @Post("/activeCard")
  async activeCard(
    @Body() activeCardDto: ActiveCardDto
  ) {
    return {
      data: await this.appService.activeCard(activeCardDto)
    };
  }

  @Post("/deactivateCard")
  async deactivateCard(
    @Body() deactivateCardDto: ActiveCardDto
  ) {
    return {
      data: await this.appService.deActiveCard(deactivateCardDto)
    };
  }

  @Post("/getTransactions")
  async getTransactions(
    @Body() getTransactionsDto: GetTransactionsDto
  ) {
    return {
      data: await this.appService.getTransaction(getTransactionsDto)
    };
  }

  @Post("/transfer/walletToWallet")
  async walletToWallet(
    @Body() walletToWalletDto: WalletToWalletDto
  ) {
    return {
      data: await this.appService.transferWalletToWallet(walletToWalletDto)
    };
  }

  @Post("/transfer/walletToIban")
  async walletToIban(
    @Body() walletToIbanDto: WalletToIbanDto
  ) {
    return {
      data: await this.appService.transferWalletToIban(walletToIbanDto)
    };
  }

  @Post("/getInfo/byNationalCode")
  async getInfoByNationalCode(
    @Body() infoByNationalCode: InfoByNationalCodeDto
  ) {
    return {
      data: await this.appService.getInfoCustomerByNationalId(infoByNationalCode.nationalCode)
    };
  }

  @Post("/getInfo/byCardNumber")
  async getInfoByCardNumber(
    @Body() infoByCardDto: InfoByCardNumberDto
  ) {
    return {
      data: await this.appService.getInfoCustomerByCardNumber(infoByCardDto.cardOrWallet)
    };
  }

  @Post("/updateCustomerAddress")
  async updateCustomerAddress(
    @Body() updateCustomerAddressDto: UpdateCustomerAddressDto
  ) {
    return {
      data: await this.appService.updateCustomerAddress(updateCustomerAddressDto)
    };
  }

  @Post("/blockMoney")
  async blockMoney(
    @Body() blockMoneyDto: BlockMoneyDto
  ) {
    return {
      data: await this.appService.blockMoney(blockMoneyDto)
    };
  }

  @Post("/showBlockMoney")
  async showBlockMoney(
    @Body() showBlockMoneyDto: ShowBlockMoneyDto
  ) {
    return {
      data: await this.appService.showBlockMoney(showBlockMoneyDto.cardNumber)
    };
  }

  @Post("/unblockMoney")
  async unblockMoney(
    @Body() unblockMoneyDto: UnblockMoneyDto
  ) {
    return {
      data: await this.appService.unBlockMoney(unblockMoneyDto)
    };
  }
}
