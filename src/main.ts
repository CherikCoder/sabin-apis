import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import * as dotenv from "dotenv";
import { Logger, ValidationPipe } from "@nestjs/common";
import { TransformInterceptor } from "./common/interceptor";
import { HandleExceptionFilter, SabinExceptionFilter } from "./common/filters";
import * as process from 'process';
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { json } from "express";

dotenv.config({
  path: ".env"
});

async function bootstrap() {
  const logger = new Logger("Sabin apis");


  const app = await NestFactory.create(AppModule);


  app.setGlobalPrefix("api");

  app.enableCors({
    methods: ["GET", "POST", "PATCH", "DELETE", "PUT"],
    preflightContinue: false
  });

  app.use((req, res, next) => {
    res.removeHeader("access-control-allow-methods");
    next();
  });

  app.useGlobalInterceptors(new TransformInterceptor());

  app.useGlobalFilters(
    new SabinExceptionFilter(),
    new HandleExceptionFilter()
  );
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: false,
      forbidNonWhitelisted: true,
      stopAtFirstError: true
    })
  );

  const config = new DocumentBuilder()
    .setTitle("Sabin apis")
    .setVersion("1.0")
    .addBearerAuth({
      name: "authorization",
      type: "http",
      scheme: "bearer",
      bearerFormat: "JWT"
    })
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("swagger/api/document", app, document);
  const port = process.env.PORT || 5000;
  app.use(json({ limit: "50mb" }));


  await app.listen(port, () => logger.log(`Server running at port: ${port}`));
}

bootstrap().then();
