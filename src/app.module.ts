import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { SabinApi } from "./common";
import { LoggerMiddleware, RemoveOptionsHeaderMiddleware } from "./common/middleware";

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, SabinApi]
})
export class AppModule implements NestModule{
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(RemoveOptionsHeaderMiddleware).forRoutes('*');
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
