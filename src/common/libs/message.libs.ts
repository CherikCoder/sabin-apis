export function getMessage(type: string, field: string): string {
  switch (type) {
    case 'empty':
      return `وارد کردن ${field} اجباری می‌باشد`;
    case 'string':
      return `${field} باید شامل حروف باشد`;
    case 'number':
      return `${field} فقط می‌تواند عدد باشد`;
  }
}
