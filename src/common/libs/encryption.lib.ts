import * as crypto from 'crypto'
import * as dotEnv from "dotenv";
dotEnv.config();


export class EncryptionLib {
    public algorithm = 'aes-256-cbc';
    public secretKey = process.env.APP_SECRET

    //TODO: delete crypto package and use nodejs build-in encryption
    encrypt(data: string) {
        try {
            const iv = crypto.randomBytes(10);
            const cipher = crypto.createCipher(this.algorithm, Buffer.from(this.secretKey), iv);
            let encrypted = cipher.update(data, 'utf-8', 'hex');
            encrypted += cipher.final('hex');
            return `${iv.toString('hex')}:${encrypted}`;
        } catch (e) {
            console.log(e)
            return ''
        }
    }

    decrypt(data: string) {
      try {
          const [ivString, encryptedText] = data.split(':');
          const iv = Buffer.from(ivString, 'hex');
          const decipher = crypto.createDecipher(this.algorithm, Buffer.from(this.secretKey), iv);
          let decrypted = decipher.update(encryptedText, 'hex', 'utf-8');
          decrypted += decipher.final('utf-8');
          return decrypted;
      }catch (e){
          console.log(e)
          return ''
      }
    }
}