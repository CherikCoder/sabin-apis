import {applyDecorators, HttpStatus} from '@nestjs/common';
import {
    ApiBadRequestResponse,
    ApiCreatedResponse, ApiForbiddenResponse,
    ApiInternalServerErrorResponse,
    ApiNotFoundResponse,
    ApiOkResponse, ApiProperty,
    ApiUnauthorizedResponse, ApiUnsupportedMediaTypeResponse,
} from '@nestjs/swagger';
import {
    BadRequestExceptionResponse,
    InternalExceptionResponse,
    NotFoundExceptionResponse,
    ForbiddenExceptionResponse,
    UnAuthorizedExceptionResponse, UnsupportedMediaTypeExceptionResponse
} from "./response-example.libs";


export interface IShowResponseParams {
    showNotFound?: boolean;
    showCreated?: boolean;
    showUnAuthorized?: boolean;
    showUnSupportedMediaType?: boolean;
    forbidden?: boolean
}

export interface ICreateExampleResponseParams {
    type?: any;
    status?: HttpStatus,
    isGetMethod?: boolean;
    isArray?: boolean;
    showResponse?: IShowResponseParams;
}

class ExceptionResponse {
    @ApiProperty({
        example: false,
        type: Boolean,
    })
    success: boolean;
}


export function ExampleResponse(params?: ICreateExampleResponseParams) {
    const decorators = [
        !params?.showResponse?.showCreated && ApiOkResponse({
            status: HttpStatus.OK,
            description: 'موفق',
            type: params?.type,
            isArray: params?.isArray || false
        }),
        params?.showResponse?.showCreated && ApiCreatedResponse({
            status: HttpStatus.CREATED,
            description: 'موفق',
            type: params?.type,
            isArray: params?.isArray || false
        }),
        params?.showResponse?.forbidden && ApiForbiddenResponse({
            status: HttpStatus.FORBIDDEN,
            description: 'خطای دسترسی به منبع',
            type: ForbiddenExceptionResponse,
        }),
        ApiBadRequestResponse({
            status: HttpStatus.BAD_REQUEST,
            description: 'ناموفق',
            type: BadRequestExceptionResponse
        }),
        (params?.showResponse?.showUnAuthorized === undefined || params?.showResponse?.showUnAuthorized) && ApiUnauthorizedResponse({
            status: HttpStatus.UNAUTHORIZED,
            description: 'خطای دسترسی-احراز هویت نشده',
            type: UnAuthorizedExceptionResponse
        }),
        (params?.showResponse?.showUnSupportedMediaType === undefined || params?.showResponse?.showUnSupportedMediaType) && ApiUnsupportedMediaTypeResponse({
            status: HttpStatus.UNSUPPORTED_MEDIA_TYPE,
            description: 'خطای نوع محتوای ارسال شده',
            type: UnsupportedMediaTypeExceptionResponse
        }),
        (params?.showResponse?.showNotFound === undefined || params?.showResponse?.showNotFound ) && ApiNotFoundResponse({
            status: HttpStatus.NOT_FOUND,
            description: 'خطای دریافت',
            type : NotFoundExceptionResponse,
        }),
        ApiInternalServerErrorResponse({
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            description: 'خطای سرور',
            type: InternalExceptionResponse
        }),
    ];

    return applyDecorators(...decorators.filter(x => x));

}


