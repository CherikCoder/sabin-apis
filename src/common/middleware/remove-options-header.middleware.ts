import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class RemoveOptionsHeaderMiddleware implements NestMiddleware {
    async use(req: Request, res: Response, next: () => void) {
        if (req.method === 'OPTIONS') {
            res.removeHeader('access-control-allow-methods')
        }
        next();
    }
}