import {Injectable, Logger, NestMiddleware} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {

        const { method, baseUrl } = req;


        new Logger('Junior Bank')
            .log(`${method.toUpperCase()} ${baseUrl}`);

        next();

    }
}
