import { Injectable, NestMiddleware, HttpException, HttpStatus } from '@nestjs/common';
import * as express from 'express';

@Injectable()
export class XmlCheckMiddleware implements NestMiddleware {
    use(req: express.Request, res: express.Response, next: express.NextFunction) {
        const contentType = req.get('Content-Type');
         if (contentType && contentType.toLowerCase() === 'application/xml') {
            throw new HttpException('فرمت اطلاعات وارد شده صحیح نمی باشد', HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
        next();
    }
}