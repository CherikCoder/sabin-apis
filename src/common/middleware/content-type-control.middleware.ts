 import {
     BadRequestException,
    Injectable,
    MethodNotAllowedException,
    NestMiddleware,
    UnsupportedMediaTypeException
} from '@nestjs/common';
import {NextFunction, Request, Response} from 'express';

@Injectable()
export class ContentTypeControlMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {

        const {method, baseUrl, headers} = req;

        const {authorization} = req.headers;

        if (!authorization)
            return next();

        if (!['GET', 'POST', 'PATCH', 'DELETE', 'PUT'].includes(method.toUpperCase())){
            throw new MethodNotAllowedException("متد ارسال شده صحیح نمی باشد")
        }

        if (["GET","DELETE"].includes(method.toUpperCase()))
            return next();

        const skipPaths = [
            'user/callback',
        ];

        if (skipPaths.find(path => baseUrl.includes(path)))
            return next();

        if (!headers["content-type"] ) {
            throw new BadRequestException('نوع محتوای ارسال شده را تعیین کنید')
        }

        if (!headers["content-type"].includes("application/json"))
            throw new UnsupportedMediaTypeException("نوع محتوای ارسال شده معتبر نمی باشد");
        next();
    }
}
