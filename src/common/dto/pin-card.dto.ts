import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from "class-validator";
import { getMessage } from "../libs";

export class PinCodeDto {
  @ApiProperty({
    type: String,
  })
  @IsOptional()
  pin?: string;


  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty({ message: getMessage('empty', 'کد ملی') })
  nationalCode: string;

  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty({ message: getMessage('empty', 'شماره کارت') })
  cardNumber: string;
}
