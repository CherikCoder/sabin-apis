import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class BaseDto {
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    clientAddress: string;

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    acceptorCode: string;
}