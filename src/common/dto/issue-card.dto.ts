import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";
import {Transform, Type} from "class-transformer";

export class IssueCardDto {
    @ApiProperty({
        type: Number,
    })
    @Type(() => Number)
    @Transform(({ value }) => parseInt(value))
    @IsNotEmpty()
    cardType: number;

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    nationalCode: string;

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    indicator: string

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    template: string
}