import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class GetTransactionsDto {
  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  CardNumber: string;

  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  FromDate: string;

  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  ToDate: string;

  @ApiProperty({
    type: String,
    required: false
  })
  @IsOptional()
  RefrenceNo?: string;

  @ApiProperty({
    type: Number,
    required: false
  })
  @IsNumber()
  @IsNotEmpty()
  Offset: number;


  @ApiProperty({
    type: Number
  })
  @IsNumber()
  @IsNotEmpty()
  Limit: number;
}