import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class SendOtpDto{
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    cardNumber: string
}