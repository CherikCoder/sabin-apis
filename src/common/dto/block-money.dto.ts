import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class BlockMoneyDto {
  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  cardNumber: string;

  @ApiProperty({
    type: Number
  })
  @IsNumber()
  @IsNotEmpty()
  amount: number;
}