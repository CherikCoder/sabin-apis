import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class InfoByNationalCodeDto {
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    nationalCode: string
}