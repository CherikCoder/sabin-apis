import {ApiProperty} from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class WalletToIbanDto  {
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    walletSource: string;

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    iban: string;

    @ApiProperty({
        type: Number,
    })
    @IsNumber()
    @IsNotEmpty()
    amount: number;

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    otpCode: string
}