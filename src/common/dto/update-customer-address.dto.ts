import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class UpdateCustomerAddressDto  {
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    address: string;

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    nationalId: string;
}