import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class WalletBalanceDto {
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    walletNumber: string
}