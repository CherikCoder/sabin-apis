import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class CreateWalletDto {

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    cif: string;

    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    iban: string
}