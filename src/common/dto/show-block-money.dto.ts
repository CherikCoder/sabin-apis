import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class ShowBlockMoneyDto{
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    cardNumber: string
}