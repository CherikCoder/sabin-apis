import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class InfoByCardNumberDto{
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    cardOrWallet: string
}