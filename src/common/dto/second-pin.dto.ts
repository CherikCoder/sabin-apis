import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { getMessage } from "../libs";

export class SecondPinDto {
    @ApiProperty({
        type: String
    })
    @IsString()
    @IsNotEmpty({ message: getMessage('empty', 'رمز اینترنتی') })
    pin: string;


    @ApiProperty({
        type: String
    })
    @IsString()
    @IsNotEmpty({ message: getMessage('empty', 'کد ملی') })
    nationalCode: string;

    @ApiProperty({
        type: String
    })
    @IsString()
    @IsNotEmpty({ message: getMessage('empty', 'شماره کارت') })
    cardNumber: string;
}
