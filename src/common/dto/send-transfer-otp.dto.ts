import {ApiProperty} from "@nestjs/swagger";
import {IsEnum, IsNotEmpty} from "class-validator";
import {Transform, Type} from "class-transformer";
import { TransferOperationType } from "../sabin";

export class SendTransferOtpDto {
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    walletSource: string

    @ApiProperty({
        enum: TransferOperationType,
        description: 'نوع عملیات باید یکی از این دو مورد باشد : 2 || 1 || 3 || 4 || 5',
    })
    @IsEnum(TransferOperationType, {
        message: 'نوع عملیات باید یکی از این دو مورد باشد :  2 || 1 || 3 || 4 || 5',
    })
    @Type(() => Number)
    @Transform(({ value }) => parseInt(value))
    @IsNotEmpty()
    operationType: TransferOperationType

}