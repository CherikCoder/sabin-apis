
import {ApiProperty} from "@nestjs/swagger";
import {IsEnum, IsNotEmpty, IsString} from "class-validator";
import {Transform, Type} from "class-transformer";
import { KindPin, TransferOperationType } from "../sabin";

export class ChangePinDto {
    @ApiProperty({
        enum: KindPin,
        description: 'نوع رمز باید یکی از این دو مورد باشد : 2 || 1',
    })
    @IsEnum(TransferOperationType, {
        message: 'نوع عملیات رمز یکی از این دو مورد باشد :  2 || 1 ',
    })
    @Type(() => Number)
    @Transform(({ value }) => parseInt(value))
    @IsNotEmpty()
    kindPin: KindPin

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    newPin: string;

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    cardNumber: string


    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    nationalCode: string
}