import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class ChargeCardDto {

  @ApiProperty({
    type: Number
  })
  @IsNumber()
  @IsNotEmpty()
  amount: number;


  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  referenceNumber: string;


  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  walletNumber: string;
}