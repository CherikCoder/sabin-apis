import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class CardBalanceDto {
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    cardNumber: string
}