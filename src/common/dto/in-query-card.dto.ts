import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty} from "class-validator";

export class InQueryCardDto {
    @ApiProperty({
        type: String,
    })
    @IsNotEmpty()
    nationalCode: string
}