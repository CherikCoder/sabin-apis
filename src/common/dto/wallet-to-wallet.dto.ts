import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class WalletToWalletDto {
  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  walletSource: string;

  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  walletDestanation: string;

  @ApiProperty({
    type: Number
  })
  @IsNumber()
  @IsNotEmpty()
  amount: number;

  @ApiProperty({
    type: String
  })
  @IsString()
  @IsNotEmpty()
  otpCode: string;
}