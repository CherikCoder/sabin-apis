import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class ActiveCardDto {
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    cardNumber: string;


    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    nationalCode: string
}