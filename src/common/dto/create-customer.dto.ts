import { IsEnum, IsNotEmpty, IsOptional } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { Type, Transform } from "class-transformer";
import { MaritalStatus, SexType } from "../sabin";

export class CreateCustomerDto {

  @ApiProperty({
    type: String
  })
  @IsNotEmpty()
  FirstName: string;

  @ApiProperty({
    type: String
  })
  @IsNotEmpty()
  LastName: string;

  @ApiProperty({
    type: String
  })
  @IsNotEmpty()
  NationalCode: string;

  @ApiProperty({
    type: String
  })
  @IsNotEmpty()
  FatherName: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  IdNumber?: string;

  @ApiProperty({
    type: String
  })
  @IsNotEmpty()
  Mobile: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  HomeAddress?: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  HomePhone?: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  HomeZipCode?: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  OfficePhone?: string;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  OfficeAddress?: string;

  @ApiProperty({
    enum: SexType,
    description: "جنسیت باید یکی از این دو مورد باشد : 2 || 1"
  })
  @IsEnum(SexType, {
    message: "جنسیت باید یکی از این دو مورد باشد : 1 || 2"
  })
  @Type(() => Number)
  @Transform(({ value }) => parseInt(value))
  @IsOptional()
  Sex?: SexType;

  @ApiProperty({
    enum: MaritalStatus,
    description: "وضعیت تاهل باید یکی از این دو مورد باشد : 2 || 1"
  })
  @IsEnum(MaritalStatus, {
    message: "وضعیت تاهل باید یکی از این دو مورد باشد : 1 || 2"
  })
  @IsNotEmpty()
  MaritalStatus?: MaritalStatus;

  @ApiProperty({
    type: String
  })
  @IsOptional()
  Email?: string;
}