import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class UnblockMoneyDto {
    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    cardNumber: string

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    amount: string;

    @ApiProperty({
        type: String,
    })
    @IsString()
    @IsNotEmpty()
    otpCode: string

}