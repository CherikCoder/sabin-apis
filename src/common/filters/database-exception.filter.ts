import {ArgumentsHost, Catch, ExceptionFilter, HttpStatus,} from '@nestjs/common';
import {BaseError} from 'sequelize';
import {Response} from 'express';

@Catch(BaseError)
export class DatabaseExceptionFilter implements ExceptionFilter {
    catch(exception: BaseError, host: ArgumentsHost): any {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        let message = exception.message;

        response.status(HttpStatus.BAD_REQUEST).json({
            success: false,
            message: message,
            statusCode: HttpStatus.BAD_REQUEST,
        });
    }
}
