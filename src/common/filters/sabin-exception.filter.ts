import {ArgumentsHost, BadRequestException, Catch, ExceptionFilter, HttpStatus} from "@nestjs/common";
import {Request, Response} from "express";
import {SabinException} from "../sabin/exception";

@Catch(SabinException)
export class SabinExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): any {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest<Request>();
        const response = ctx.getResponse<Response>();
        let message = exception.message;
        const name = exception.name;

        console.log({exception: name});
        console.log({exceptionData: message});

        if (message == 'getaddrinfo EAI_AGAIN cardservice.sabinarya.com') {
            console.log('Can not call Sabin API')
            message = 'خطایی پیش آمده، لطفاً دوباره امتحان کنید';
        }

        return response.status(HttpStatus.METHOD_NOT_ALLOWED).json({
            success: false,
            message: message || 'عدم دسترسی به سرویس مربوطه',
            statusCode: HttpStatus.METHOD_NOT_ALLOWED,
        });
    }
}
