export interface IBlockMoney {
    cardNumber: string;
    amount: number;
}