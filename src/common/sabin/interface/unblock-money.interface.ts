export interface IUnblockMoney {
    cardNumber: string;
    amount: string;
    otpCode: string
}