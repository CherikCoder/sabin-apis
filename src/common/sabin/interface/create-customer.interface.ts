export enum SexType {
  Male = 1,
  Female = 2,
}


export enum MaritalStatus {
  Married = '1',
  Single = '2',
}

export interface ICreateCustomer {
  FirstName: string;
  LastName: string;
  NationalCode: string;
  FatherName: string;
  IdNumber?: string;
  Mobile: string;
  HomePhone?: string;
  HomeAddress?: string;
  HomeZipCode?: string;
  OfficePhone?: string;
  OfficeAddress?: string;
  Sex?: SexType;
  MaritalStatus?: MaritalStatus;
  Email?: string;
}
