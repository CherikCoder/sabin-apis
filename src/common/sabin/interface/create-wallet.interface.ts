export interface ICreateWallet {
  cif: string;
  iban: string;
}
