export interface IGetTransaction {
  CardNumber: string;
  FromDate: string;
  ToDate: string;
  RefrenceNo?: string;
  Offset: number;
  Limit: number;
}
