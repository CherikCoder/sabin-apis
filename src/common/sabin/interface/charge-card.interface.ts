// export interface IChargeCard {
//   cardNumber: string;
//   amount: number;
//   sourceDeposit: string;
//   nationalCode: string;
// }

export interface IChargeCard {
    amount: number;
    referenceNumber: string;
  walletNumber: string;
}