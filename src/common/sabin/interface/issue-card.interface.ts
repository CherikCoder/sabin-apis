export interface IIssueCard {
  cardType: number;
  nationalCode: string;
  indicator: string;
  template: string
}
