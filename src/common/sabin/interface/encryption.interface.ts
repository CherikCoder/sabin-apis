import { Observable } from 'rxjs';

export interface IParam {
  key: string;
  plainText: string;
}

export interface IZDecryptParam {
   key : string;
   encrypted : string;
}

export interface IEncryptionService {
  Encrypt(param: IParam): Observable<IEncryptionResponse>;
  IZEncrypt(param: IParam): Observable<IIZEncryptionResponse>;
  IZDecrypt(param: IZDecryptParam) : Observable<IIZDecryptionResponse>
  IZSign(param: IParam): Observable<IIZEncryptionResponse>;
}

export interface IEncryptionResponse {
  envelope: string;
  envelopeIV: string;
  credentialData: string;
}

export interface IIZEncryptionResponse {
  encryptData: string;

}
export interface IIZDecryptionResponse {
   decrypted : string;
}

