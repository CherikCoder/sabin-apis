export interface IActiveCard {
  cardNumber: string;
  nationalCode: string;
}
