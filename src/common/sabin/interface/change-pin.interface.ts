export enum KindPin {
  FIRST_PIN = 1,
  INTERNET_PIN = 2,
}

export interface IChangePin {
  kindPin: KindPin;
  newPin: string;
  cardNumber: string;
  nationalCode: string;
}
