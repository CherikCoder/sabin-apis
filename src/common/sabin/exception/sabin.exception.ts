import {
    BadRequestException,
    GatewayTimeoutException,
    InternalServerErrorException,
    NotFoundException
} from "@nestjs/common";

export class SabinException extends Error {
    constructor(msg: string) {
        super(msg);

        Object.setPrototypeOf(this, SabinException.prototype);
        this.name = 'SabinException';
        // exceptionData: 'getaddrinfo EAI_AGAIN cardservice.sabinarya.com'
        if (msg['response'] && msg['response']['data']){
            /*internal server error : there is a problem in services or server*/
            this.message = msg['response']['data']['message'];
        } else {
            const code = msg['statusCode'];
            if (code && code != 0){

                if (code == 2) {
                    throw new GatewayTimeoutException('اتصال با سرور برقرار نشد')
                }

                if (code == 36) {
                    throw new BadRequestException('موجودی حساب کافی نمی باشد')
                }

                if (code == 32) { /*These errors related to wallet creating or transfer*/
                    throw new BadRequestException('خطایی پیش آمده، لطفاً دقایقی دیگر مجدداً تلاش کنید')
                }

                /*These errors comes from user's data*/
                const badReqArray = [20, 24, 15, 13, 32, 1, 42]; /* Error messages that are directly displayed to the user */

                 if (badReqArray.includes(code)) {
                    throw new BadRequestException(`${msg['message']}`)
                }

                const notFoundArray = [39, 43, 46, 3] /*Not found errors that are directly displayed to the user*/
                if(notFoundArray.includes(code)) {
                    throw new NotFoundException(`${msg['message']}`)
                }

                if(code == 17) {
                    throw new BadRequestException(`مسدود سازی کارت با خطا مواجه شد`)
                }

                if (code == 22 || code == 41) {
                    throw new InternalServerErrorException('خطا در ارسال پیامک')
                }

                if (code == 34 || code == 30 || code == 38) {
                    /*For card, transactions and  balance*/
                    throw new BadRequestException('استعلام با خطا مواجه شد')
                }

                if (code == 7) {
                    throw new GatewayTimeoutException(' زمان درخواست منقضی شده است, مجدداً تلاش کنید')
                }
            }

            this.message = msg['message'];
        }
    }
}
