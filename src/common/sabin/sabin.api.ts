import {Injectable, OnModuleInit} from '@nestjs/common';
import {
    IActiveCard,
    IChargeCard,
    ICreateWallet,
    IIssueCard,
    IResponseApi,
    ITransferOtp,
    ICreateCustomer,
    IChangePin,
    IGetTransaction,
    ITransferWalletToWallet,
    ITransferWalletToIban, IUpdateCustomerAddress, IBlockMoney, IUnblockMoney,
} from './interface';
import {apis} from './api';
import {SabinService} from './sabin.service';
import {Client, ClientGrpc, Transport} from '@nestjs/microservices';
import {join} from 'path';
import * as dotEnv from 'dotenv';
import {SabinException} from "./exception";

dotEnv.config();

@Injectable()
export class SabinApi extends SabinService implements OnModuleInit {

    @Client({
        transport: Transport.GRPC,
        options: {
            url: process.env.ENCRYPTION_SERVICE_URL,
            package: 'sabin',
            protoPath: join(__dirname, '../../../sabin.proto'),
        },
    })
    private client: ClientGrpc;

    onModuleInit(): any {
        this.initialize(this.client);
    }



    async createCustomer(args: ICreateCustomer): Promise<IResponseApi> {
        try {
            console.log("create customer args",args);
            const response = await this.request.post(apis.CREATE_CUSTOMER, {
                ...args,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            console.log("create customer res",response.data);
            return response.data;
        } catch (e) {
            console.log("create customer error",e);
            throw new SabinException(e);
        }
    }

    async issueCard(args: IIssueCard): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.ISSUE_CARD, {
                ...args,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e);
        }
    }

    async chargeCard(args: IChargeCard): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.CHARGE_BY_REFERENCE, {
                CredentialData: `${args.walletNumber}|${args.amount}|${args.referenceNumber}`
            })

            if (!response.data.isSuccess) {
                throw response.data;
            }
            console.log(response.data);
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e);
        }
    }

    async activeCard(args: IActiveCard) {
        return this.activeOrDeActiveCard(args, true);
    }

    async deActiveCard(args: IActiveCard) {
        return this.activeOrDeActiveCard(args, false);
    }

    private async activeOrDeActiveCard(args: IActiveCard, status: boolean): Promise<IResponseApi> {
        try {
            const url = status ? apis.ACTIVE_CARD : apis.DE_ACTIVE_CARD;

            const response = await this.request.post(url, {
                CredentialData: args.cardNumber,
                NationalCode: args.nationalCode,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e);
        }
    }

    async sendOtp(cardNumber: string): Promise<IResponseApi> {
        try {

            const response = await this.request.post(apis.SEND_OTP, {
                CredentialData: cardNumber,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e);
        }
    }

    async cardBalance(cardNumber: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.CARD_BALANCE, {
                CredentialData: cardNumber,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e)
            throw new SabinException(e);
        }
    }

    async walletBalance(walletNumber: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.WALLET_BALANCE, {
                CredentialData: walletNumber,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e)
            throw new SabinException(e);
        }
    }

    async createWallet(args: ICreateWallet): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.CREATE_WALLET, {
                ...args,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e)
            throw new SabinException(e)
        }
    }

    async getCustomerInfo(wallerOrCard: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.GET_CUSTOMER_INFO, {
                CredentialData: wallerOrCard,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e)
        }
    }

    async changePin(args: IChangePin) {
        try{
            return this.request.post(apis.CHANGE_PIN, {...args});
        }catch (e) {
            console.log(e)
            throw new SabinException(e)
        }
    }

    async sendTransferOtp(args: ITransferOtp): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.SEND_TRANSFER_OTP, {
                OperationType: args.operationType,
                CredentialData: args.walletSource,
            });


            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e)
            throw new SabinException(e)
        }
    }

    async changeCardPin(args: IChangePin): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.CHANGE_PIN, {
                CredentialData: `${args.kindPin}|${args.newPin}|${args.cardNumber}`,
                NationalCode: args.nationalCode,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e)
        }
    }

    async getTransaction(args: IGetTransaction): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.GET_TRANSACTION, {
                CredentialData: args.CardNumber,
                ...args,
                CardNumber: undefined,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            throw new SabinException(e)
        }
    }

    async transferWalletToWallet(args: ITransferWalletToWallet): Promise<IResponseApi> {
        try {

            const response = await this.request.post(apis.TRANSFER_WALLET_TO_WALLET, {
                CredentialData: `${args.walletSource}|${args.walletDestanation}|${args.amount}|${args.otpCode}`,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }

            return response.data;
        } catch (e) {
            console.log(e)
            throw new SabinException(e)

        }
    }

    async transferWalletToIban(args: ITransferWalletToIban): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.TRANSFER_WALLET_TO_IBAN, {
                CredentialData: `${args.walletSource}|${args.iban}|${args.amount}|${args.otpCode}`,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async inQueryCard(NationalCode: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.IN_QUERY_CARD, {
                NationalCode,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async getInfoCustomerByNationalId(nationalId: string) /*Promise<IResponseApi>*/ {
        try {
            const response = await this.request.post(apis.GET_INFO_CUSTOMER_BY_NATIONAL_ID, {
                CredentialData: nationalId,
            });


            if (!response.data.isSuccess) {
                if (response.data.statusCode == 45) {
                    //user does not exist
                    return false
                }
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async getInfoCustomerByCardNumber(wallerOrCard: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.GET_INFO_CUSTOMER_BY_CARD_NUMBER, {
                CredentialData: wallerOrCard,
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async updateCustomerAddress(args: IUpdateCustomerAddress): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.UPDATE_CUSTOMER_ADDRESS, {
                CredentialData: args.nationalId,
                Address: args.address
            });

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async blockMoney(args: IBlockMoney): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.BLOCK_MONEY, {
                CredentialData: `${args.cardNumber}|${args.amount}`
            })

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async unBlockMoney(args: IUnblockMoney): Promise<IResponseApi> {
        try {

            const response = await this.request.post(apis.UNBLOCK_MONEY, {
                CredentialData: `${args.cardNumber}|1111|${args.otpCode}|1111`
            })

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

    async showBlockMoney(cardNumber: string): Promise<IResponseApi> {
        try {
            const response = await this.request.post(apis.SHOW_BLOCK_MONEY, {
                CredentialData: cardNumber
            })

            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        } catch (e) {
            console.log(e);
            throw new SabinException(e)
        }
    }

}
