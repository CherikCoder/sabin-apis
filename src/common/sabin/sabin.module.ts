import { Module } from '@nestjs/common';
import { SabinApi } from './sabin.api';

@Module({
  providers: [SabinApi],
})
export class SabinModule {}
