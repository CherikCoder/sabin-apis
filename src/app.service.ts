import { BadRequestException, Injectable } from "@nestjs/common";
import {
  IActiveCard,
  IBlockMoney,
  IChargeCard,
  ICreateCustomer,
  ICreateWallet,
  IGetTransaction,
  IIssueCard,
  IResponseApi,
  ITransferOtp,
  ITransferWalletToIban,
  ITransferWalletToWallet,
  IUnblockMoney,
  IUpdateCustomerAddress,
  KindPin,
  SabinApi
} from "./common";
import { SabinException } from "./common/sabin/exception";
import { PinCodeDto } from "./common/dto/pin-card.dto";
import { SecondPinDto } from "./common/dto/second-pin.dto";

@Injectable()
export class AppService {

  constructor(private readonly sabinApi: SabinApi) {

  }

  async createCustomer(args: ICreateCustomer): Promise<IResponseApi> {

    const response = await this.sabinApi.createCustomer({
      ...args
    });

    return response.data;
  }

  async issueCard(args: IIssueCard): Promise<IResponseApi> {
    const response = await this.sabinApi.issueCard({ ...args });
    return response.data;
  }

  async chargeCard(args: IChargeCard): Promise<IResponseApi> {
    const response = await this.sabinApi.chargeCard({ ...args });
    return response.data;
  }

  async activeCard(args: IActiveCard) {
    return this.sabinApi.activeCard(args);
  }

  async deActiveCard(args: IActiveCard) {
    return this.sabinApi.deActiveCard(args);
  }

  async sendOtp(cardNumber: string): Promise<IResponseApi> {
    const response = await this.sabinApi.sendOtp(cardNumber);
    return response.data;
  }

  async cardBalance(cardNumber: string): Promise<IResponseApi> {
    const response = await this.sabinApi.cardBalance(cardNumber);
    return response.data;

  }

  async walletBalance(walletNumber: string): Promise<IResponseApi> {
    const response = await this.sabinApi.walletBalance(walletNumber);
    return response.data;
  }


  async changeSecondPin(newPin: SecondPinDto) {
    try {
      await this.sabinApi.changeCardPin({
        kindPin: KindPin.INTERNET_PIN,
        newPin: newPin.pin,
        nationalCode: newPin.nationalCode,
        cardNumber: newPin.cardNumber
      });
      return "رمز اول کارت با موفقیت تغییر کرد!";
    } catch (e) {
      throw e;
    }
  }

  async changeFirstPin(newPinDto: PinCodeDto): Promise<string> {
    try {
      await this.sabinApi.changeCardPin({
        kindPin: KindPin.FIRST_PIN,
        newPin: newPinDto.pin,
        nationalCode: newPinDto.nationalCode,
        cardNumber: newPinDto.cardNumber
      });

      return "رمز اول کارت با موفقیت تغییر کرد!";
    } catch (e) {
      throw e;
    }
  }

  async createWallet(args: ICreateWallet): Promise<IResponseApi> {
    const response = await this.sabinApi.createWallet({ ...args });
    return response.data;
  }


  async sendTransferOtp(args: ITransferOtp): Promise<IResponseApi> {
    try {
      const response = await this.sabinApi.sendTransferOtp({ ...args });


      if (!response.data.isSuccess) {
        throw response.data;
      }
      return response.data;
    } catch (e) {
      console.log(e);
      throw new SabinException(e);
    }
  }

  async getTransaction(args: IGetTransaction): Promise<IResponseApi> {
    const response = await this.sabinApi.getTransaction({ ...args });
    return response.data;
  }

  async transferWalletToWallet(args: ITransferWalletToWallet): Promise<IResponseApi> {
    const response = await this.sabinApi.transferWalletToWallet({ ...args });
    return response.data;
  }

  async transferWalletToIban(args: ITransferWalletToIban): Promise<IResponseApi> {
    const response = await this.sabinApi.transferWalletToIban({ ...args });
    return response.data;
  }

  async inQueryCard(NationalCode: string): Promise<IResponseApi> {
    const response = await this.sabinApi.inQueryCard(NationalCode);
    return response.data;
  }

  async getInfoCustomerByNationalId(nationalId: string) /*Promise<IResponseApi>*/ {
    const response = await this.sabinApi.getInfoCustomerByNationalId(nationalId);
    return response.data;
  }

  async getInfoCustomerByCardNumber(wallerOrCard: string): Promise<IResponseApi> {
    const response = await this.sabinApi.getInfoCustomerByCardNumber(wallerOrCard);
    return response.data;
  }

  async updateCustomerAddress(args: IUpdateCustomerAddress): Promise<IResponseApi> {
    const response = await this.sabinApi.updateCustomerAddress({ ...args });
    return response.data;
  }

  async blockMoney(args: IBlockMoney): Promise<IResponseApi> {
    const response = await this.sabinApi.blockMoney({ ...args });
    return response.data;
  }

  async unBlockMoney(args: IUnblockMoney): Promise<IResponseApi> {
    const response = await this.sabinApi.unBlockMoney({ ...args });
    return response.data;
  }

  async showBlockMoney(cardNumber: string): Promise<IResponseApi> {
    const response = await this.sabinApi.showBlockMoney(cardNumber);
    return response.data;
  }
}
