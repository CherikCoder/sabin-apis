"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const dotenv = require("dotenv");
const common_1 = require("@nestjs/common");
const interceptor_1 = require("./common/interceptor");
const filters_1 = require("./common/filters");
const process = require("process");
const swagger_1 = require("@nestjs/swagger");
const express_1 = require("express");
dotenv.config({
    path: ".env"
});
async function bootstrap() {
    const logger = new common_1.Logger("Sabin apis");
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.setGlobalPrefix("api");
    app.enableCors({
        methods: ["GET", "POST", "PATCH", "DELETE", "PUT"],
        preflightContinue: false
    });
    app.use((req, res, next) => {
        res.removeHeader("access-control-allow-methods");
        next();
    });
    app.useGlobalInterceptors(new interceptor_1.TransformInterceptor());
    app.useGlobalFilters(new filters_1.SabinExceptionFilter(), new filters_1.HandleExceptionFilter());
    app.useGlobalPipes(new common_1.ValidationPipe({
        whitelist: false,
        forbidNonWhitelisted: true,
        stopAtFirstError: true
    }));
    const config = new swagger_1.DocumentBuilder()
        .setTitle("Sabin apis")
        .setVersion("1.0")
        .addBearerAuth({
        name: "authorization",
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT"
    })
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup("swagger/api/document", app, document);
    const port = process.env.PORT || 5000;
    app.use((0, express_1.json)({ limit: "50mb" }));
    await app.listen(port, () => logger.log(`Server running at port: ${port}`));
}
bootstrap().then();
//# sourceMappingURL=main.js.map