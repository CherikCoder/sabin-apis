export declare class CreateWalletDto {
    cif: string;
    iban: string;
}
