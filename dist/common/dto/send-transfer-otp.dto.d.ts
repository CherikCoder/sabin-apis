import { TransferOperationType } from "../sabin";
export declare class SendTransferOtpDto {
    walletSource: string;
    operationType: TransferOperationType;
}
