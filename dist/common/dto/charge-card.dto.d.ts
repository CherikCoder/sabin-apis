export declare class ChargeCardDto {
    amount: number;
    referenceNumber: string;
    walletNumber: string;
}
