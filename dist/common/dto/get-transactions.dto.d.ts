export declare class GetTransactionsDto {
    CardNumber: string;
    FromDate: string;
    ToDate: string;
    RefrenceNo?: string;
    Offset: number;
    Limit: number;
}
