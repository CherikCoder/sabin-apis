"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendTransferOtpDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const sabin_1 = require("../sabin");
class SendTransferOtpDto {
}
exports.SendTransferOtpDto = SendTransferOtpDto;
__decorate([
    (0, swagger_1.ApiProperty)({
        type: String,
    }),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", String)
], SendTransferOtpDto.prototype, "walletSource", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        enum: sabin_1.TransferOperationType,
        description: 'نوع عملیات باید یکی از این دو مورد باشد : 2 || 1 || 3 || 4 || 5',
    }),
    (0, class_validator_1.IsEnum)(sabin_1.TransferOperationType, {
        message: 'نوع عملیات باید یکی از این دو مورد باشد :  2 || 1 || 3 || 4 || 5',
    }),
    (0, class_transformer_1.Type)(() => Number),
    (0, class_transformer_1.Transform)(({ value }) => parseInt(value)),
    (0, class_validator_1.IsNotEmpty)(),
    __metadata("design:type", Number)
], SendTransferOtpDto.prototype, "operationType", void 0);
//# sourceMappingURL=send-transfer-otp.dto.js.map