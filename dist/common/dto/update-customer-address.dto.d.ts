export declare class UpdateCustomerAddressDto {
    address: string;
    nationalId: string;
}
