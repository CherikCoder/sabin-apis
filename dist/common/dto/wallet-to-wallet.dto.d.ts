export declare class WalletToWalletDto {
    walletSource: string;
    walletDestanation: string;
    amount: number;
    otpCode: string;
}
