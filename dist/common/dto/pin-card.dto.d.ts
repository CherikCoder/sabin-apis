export declare class PinCodeDto {
    pin?: string;
    nationalCode: string;
    cardNumber: string;
}
