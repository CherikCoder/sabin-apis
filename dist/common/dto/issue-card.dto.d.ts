export declare class IssueCardDto {
    cardType: number;
    nationalCode: string;
    indicator: string;
    template: string;
}
