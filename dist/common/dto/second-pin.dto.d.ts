export declare class SecondPinDto {
    pin: string;
    nationalCode: string;
    cardNumber: string;
}
