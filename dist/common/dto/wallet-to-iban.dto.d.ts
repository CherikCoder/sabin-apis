export declare class WalletToIbanDto {
    walletSource: string;
    iban: string;
    amount: number;
    otpCode: string;
}
