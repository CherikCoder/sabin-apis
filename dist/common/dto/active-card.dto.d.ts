export declare class ActiveCardDto {
    cardNumber: string;
    nationalCode: string;
}
