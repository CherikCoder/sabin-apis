import { KindPin } from "../sabin";
export declare class ChangePinDto {
    kindPin: KindPin;
    newPin: string;
    cardNumber: string;
    nationalCode: string;
}
