import { MaritalStatus, SexType } from "../sabin";
export declare class CreateCustomerDto {
    FirstName: string;
    LastName: string;
    NationalCode: string;
    FatherName: string;
    IdNumber?: string;
    Mobile: string;
    HomeAddress?: string;
    HomePhone?: string;
    HomeZipCode?: string;
    OfficePhone?: string;
    OfficeAddress?: string;
    Sex?: SexType;
    MaritalStatus?: MaritalStatus;
    Email?: string;
}
