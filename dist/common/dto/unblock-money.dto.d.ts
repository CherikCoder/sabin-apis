export declare class UnblockMoneyDto {
    cardNumber: string;
    amount: string;
    otpCode: string;
}
