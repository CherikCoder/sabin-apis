"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HandleExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
let HandleExceptionFilter = class HandleExceptionFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const status = exception.getStatus();
        if (exception instanceof common_1.NotFoundException && exception.message.includes('Cannot')) {
            const path = exception.message.substring(exception.message.indexOf('/'));
            if (this.existsRoutePath(request, path))
                return response.status(common_1.HttpStatus.METHOD_NOT_ALLOWED).json({
                    success: false,
                    message: 'سرویس مجاز نمی باشد',
                    statusCode: common_1.HttpStatus.METHOD_NOT_ALLOWED,
                });
            return response.status(status).json({
                success: false,
                message: 'منبع درخواستی یافت نشد',
                statusCode: status,
            });
        }
        if (exception instanceof common_1.BadRequestException &&
            new RegExp(/(unexpected)(.*?)(json)/g).test(exception.message.toLowerCase())) {
            return response.status(status).json({
                success: false,
                message: 'فرمت JSON اطلاعات ارسال شده صحبح نمی باشد',
                statusCode: status,
            });
        }
        let message = 'عملیات با موفقیت انجام نشد';
        if (exception.response?.message &&
            Array.isArray(exception.response.message))
            message = exception.response.message[0];
        else if (exception.message)
            message = exception.message;
        response.status(status).json({
            success: false,
            message,
            statusCode: status,
        });
    }
    existsRoutePath(request, path) {
        let servicePath = path;
        if (path.includes('?'))
            servicePath = path.substring(0, path.indexOf('?'));
        try {
            const router = request.app._router;
            const existPaths = router.stack
                .map((layer) => {
                if (layer.route) {
                    const path = layer.route?.path;
                    const method = layer.route?.stack[0].method;
                    return { method: method.toUpperCase(), path };
                }
            })
                .filter((item) => item !== undefined)
                .filter((item) => item.path === servicePath);
            return existPaths.length > 0;
        }
        catch (e) {
            throw new Error('');
        }
    }
};
exports.HandleExceptionFilter = HandleExceptionFilter;
exports.HandleExceptionFilter = HandleExceptionFilter = __decorate([
    (0, common_1.Catch)(common_1.HttpException)
], HandleExceptionFilter);
//# sourceMappingURL=handle-exception.filter.js.map