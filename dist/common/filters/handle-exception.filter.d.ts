import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
export declare class HandleExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): any;
    private existsRoutePath;
}
