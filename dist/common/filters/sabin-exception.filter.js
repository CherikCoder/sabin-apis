"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SabinExceptionFilter = void 0;
const common_1 = require("@nestjs/common");
const exception_1 = require("../sabin/exception");
let SabinExceptionFilter = class SabinExceptionFilter {
    catch(exception, host) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        let message = exception.message;
        const name = exception.name;
        console.log({ exception: name });
        console.log({ exceptionData: message });
        if (message == 'getaddrinfo EAI_AGAIN cardservice.sabinarya.com') {
            console.log('Can not call Sabin API');
            message = 'خطایی پیش آمده، لطفاً دوباره امتحان کنید';
        }
        return response.status(common_1.HttpStatus.METHOD_NOT_ALLOWED).json({
            success: false,
            message: message || 'عدم دسترسی به سرویس مربوطه',
            statusCode: common_1.HttpStatus.METHOD_NOT_ALLOWED,
        });
    }
};
exports.SabinExceptionFilter = SabinExceptionFilter;
exports.SabinExceptionFilter = SabinExceptionFilter = __decorate([
    (0, common_1.Catch)(exception_1.SabinException)
], SabinExceptionFilter);
//# sourceMappingURL=sabin-exception.filter.js.map