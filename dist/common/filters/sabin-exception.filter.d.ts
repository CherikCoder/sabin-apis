import { ArgumentsHost, ExceptionFilter } from "@nestjs/common";
export declare class SabinExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost): any;
}
