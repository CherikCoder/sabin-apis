import { ArgumentsHost, ExceptionFilter } from '@nestjs/common';
import { BaseError } from 'sequelize';
export declare class DatabaseExceptionFilter implements ExceptionFilter {
    catch(exception: BaseError, host: ArgumentsHost): any;
}
