"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SabinApi = void 0;
const common_1 = require("@nestjs/common");
const api_1 = require("./api");
const sabin_service_1 = require("./sabin.service");
const microservices_1 = require("@nestjs/microservices");
const path_1 = require("path");
const dotEnv = require("dotenv");
const exception_1 = require("./exception");
dotEnv.config();
let SabinApi = class SabinApi extends sabin_service_1.SabinService {
    onModuleInit() {
        this.initialize(this.client);
    }
    async createCustomer(args) {
        try {
            console.log("create customer args", args);
            const response = await this.request.post(api_1.apis.CREATE_CUSTOMER, {
                ...args,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            console.log("create customer res", response.data);
            return response.data;
        }
        catch (e) {
            console.log("create customer error", e);
            throw new exception_1.SabinException(e);
        }
    }
    async issueCard(args) {
        try {
            const response = await this.request.post(api_1.apis.ISSUE_CARD, {
                ...args,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async chargeCard(args) {
        try {
            const response = await this.request.post(api_1.apis.CHARGE_BY_REFERENCE, {
                CredentialData: `${args.walletNumber}|${args.amount}|${args.referenceNumber}`
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            console.log(response.data);
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async activeCard(args) {
        return this.activeOrDeActiveCard(args, true);
    }
    async deActiveCard(args) {
        return this.activeOrDeActiveCard(args, false);
    }
    async activeOrDeActiveCard(args, status) {
        try {
            const url = status ? api_1.apis.ACTIVE_CARD : api_1.apis.DE_ACTIVE_CARD;
            const response = await this.request.post(url, {
                CredentialData: args.cardNumber,
                NationalCode: args.nationalCode,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async sendOtp(cardNumber) {
        try {
            const response = await this.request.post(api_1.apis.SEND_OTP, {
                CredentialData: cardNumber,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async cardBalance(cardNumber) {
        try {
            const response = await this.request.post(api_1.apis.CARD_BALANCE, {
                CredentialData: cardNumber,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async walletBalance(walletNumber) {
        try {
            const response = await this.request.post(api_1.apis.WALLET_BALANCE, {
                CredentialData: walletNumber,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async createWallet(args) {
        try {
            const response = await this.request.post(api_1.apis.CREATE_WALLET, {
                ...args,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async getCustomerInfo(wallerOrCard) {
        try {
            const response = await this.request.post(api_1.apis.GET_CUSTOMER_INFO, {
                CredentialData: wallerOrCard,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async changePin(args) {
        return this.request.post(api_1.apis.CHANGE_PIN, { ...args });
    }
    async sendTransferOtp(args) {
        try {
            const response = await this.request.post(api_1.apis.SEND_TRANSFER_OTP, {
                OperationType: args.operationType,
                CredentialData: args.walletSource,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async changeCardPin(args) {
        try {
            const response = await this.request.post(api_1.apis.CHANGE_PIN, {
                CredentialData: `${args.kindPin}|${args.newPin}|${args.cardNumber}`,
                NationalCode: args.nationalCode,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async getTransaction(args) {
        try {
            const response = await this.request.post(api_1.apis.GET_TRANSACTION, {
                CredentialData: args.CardNumber,
                ...args,
                CardNumber: undefined,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            throw new exception_1.SabinException(e);
        }
    }
    async transferWalletToWallet(args) {
        try {
            const response = await this.request.post(api_1.apis.TRANSFER_WALLET_TO_WALLET, {
                CredentialData: `${args.walletSource}|${args.walletDestanation}|${args.amount}|${args.otpCode}`,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async transferWalletToIban(args) {
        try {
            const response = await this.request.post(api_1.apis.TRANSFER_WALLET_TO_IBAN, {
                CredentialData: `${args.walletSource}|${args.iban}|${args.amount}|${args.otpCode}`,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async inQueryCard(NationalCode) {
        try {
            const response = await this.request.post(api_1.apis.IN_QUERY_CARD, {
                NationalCode,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async getInfoCustomerByNationalId(nationalId) {
        try {
            const response = await this.request.post(api_1.apis.GET_INFO_CUSTOMER_BY_NATIONAL_ID, {
                CredentialData: nationalId,
            });
            if (!response.data.isSuccess) {
                if (response.data.statusCode == 45) {
                    return false;
                }
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async getInfoCustomerByCardNumber(wallerOrCard) {
        try {
            const response = await this.request.post(api_1.apis.GET_INFO_CUSTOMER_BY_CARD_NUMBER, {
                CredentialData: wallerOrCard,
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async updateCustomerAddress(args) {
        try {
            const response = await this.request.post(api_1.apis.UPDATE_CUSTOMER_ADDRESS, {
                CredentialData: args.nationalId,
                Address: args.address
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async blockMoney(args) {
        try {
            const response = await this.request.post(api_1.apis.BLOCK_MONEY, {
                CredentialData: `${args.cardNumber}|${args.amount}`
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async unBlockMoney(args) {
        try {
            const response = await this.request.post(api_1.apis.UNBLOCK_MONEY, {
                CredentialData: `${args.cardNumber}|1111|${args.otpCode}|1111`
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async showBlockMoney(cardNumber) {
        try {
            const response = await this.request.post(api_1.apis.SHOW_BLOCK_MONEY, {
                CredentialData: cardNumber
            });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
};
exports.SabinApi = SabinApi;
__decorate([
    (0, microservices_1.Client)({
        transport: microservices_1.Transport.GRPC,
        options: {
            url: process.env.ENCRYPTION_SERVICE_URL,
            package: 'sabin',
            protoPath: (0, path_1.join)(__dirname, '../../../sabin.proto'),
        },
    }),
    __metadata("design:type", Object)
], SabinApi.prototype, "client", void 0);
exports.SabinApi = SabinApi = __decorate([
    (0, common_1.Injectable)()
], SabinApi);
//# sourceMappingURL=sabin.api.js.map