"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferOperationType = void 0;
var TransferOperationType;
(function (TransferOperationType) {
    TransferOperationType[TransferOperationType["WALLET_TO_WALLET"] = 1] = "WALLET_TO_WALLET";
    TransferOperationType[TransferOperationType["WALLET_TO_IBAN"] = 2] = "WALLET_TO_IBAN";
    TransferOperationType[TransferOperationType["UNBLOCK_MONEY"] = 5] = "UNBLOCK_MONEY";
})(TransferOperationType || (exports.TransferOperationType = TransferOperationType = {}));
//# sourceMappingURL=transfer-otp.interface.js.map