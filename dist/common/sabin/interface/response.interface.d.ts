export interface IResponseApi {
    data: any;
    isSuccess: boolean;
    statusCode: number;
    dateTime: string;
    message: string;
}
