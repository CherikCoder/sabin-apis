export declare enum TransferOperationType {
    WALLET_TO_WALLET = 1,
    WALLET_TO_IBAN = 2,
    UNBLOCK_MONEY = 5
}
export interface ITransferOtp {
    walletSource: string;
    operationType: TransferOperationType;
}
