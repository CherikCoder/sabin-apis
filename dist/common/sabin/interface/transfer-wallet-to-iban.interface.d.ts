export interface ITransferWalletToIban {
    walletSource: string;
    iban: string;
    amount: number;
    otpCode: string;
}
