export interface IUpdateCustomerAddress {
    nationalId: string;
    address: string;
}
