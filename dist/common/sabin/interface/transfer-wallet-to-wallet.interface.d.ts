export interface ITransferWalletToWallet {
    walletSource: string;
    walletDestanation: string;
    amount: number;
    otpCode: string;
}
