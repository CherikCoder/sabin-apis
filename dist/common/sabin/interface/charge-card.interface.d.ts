export interface IChargeCard {
    amount: number;
    referenceNumber: string;
    walletNumber: string;
}
