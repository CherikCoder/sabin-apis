"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MaritalStatus = exports.SexType = void 0;
var SexType;
(function (SexType) {
    SexType[SexType["Male"] = 1] = "Male";
    SexType[SexType["Female"] = 2] = "Female";
})(SexType || (exports.SexType = SexType = {}));
var MaritalStatus;
(function (MaritalStatus) {
    MaritalStatus["Married"] = "1";
    MaritalStatus["Single"] = "2";
})(MaritalStatus || (exports.MaritalStatus = MaritalStatus = {}));
//# sourceMappingURL=create-customer.interface.js.map