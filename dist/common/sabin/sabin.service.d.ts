import { ClientGrpc } from '@nestjs/microservices';
import { IEncryptionService } from './interface';
import { AxiosInstance } from 'axios';
export declare class SabinService {
    protected encryptionService: IEncryptionService;
    protected request: AxiosInstance;
    private privateKey;
    private publicKey;
    protected initialize(client: ClientGrpc): void;
    private getAuthorizationHeader;
    private makeSignature;
    private decryptData;
}
