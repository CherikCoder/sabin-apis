"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SabinException = void 0;
const common_1 = require("@nestjs/common");
class SabinException extends Error {
    constructor(msg) {
        super(msg);
        Object.setPrototypeOf(this, SabinException.prototype);
        this.name = 'SabinException';
        if (msg['response'] && msg['response']['data']) {
            this.message = msg['response']['data']['message'];
        }
        else {
            const code = msg['statusCode'];
            if (code && code != 0) {
                if (code == 2) {
                    throw new common_1.GatewayTimeoutException('اتصال با سرور برقرار نشد');
                }
                if (code == 36) {
                    throw new common_1.BadRequestException('موجودی حساب کافی نمی باشد');
                }
                if (code == 32) {
                    throw new common_1.BadRequestException('خطایی پیش آمده، لطفاً دقایقی دیگر مجدداً تلاش کنید');
                }
                const badReqArray = [20, 24, 15, 13, 32, 1, 42];
                if (badReqArray.includes(code)) {
                    throw new common_1.BadRequestException(`${msg['message']}`);
                }
                const notFoundArray = [39, 43, 46, 3];
                if (notFoundArray.includes(code)) {
                    throw new common_1.NotFoundException(`${msg['message']}`);
                }
                if (code == 17) {
                    throw new common_1.BadRequestException(`مسدود سازی کارت با خطا مواجه شد`);
                }
                if (code == 22 || code == 41) {
                    throw new common_1.InternalServerErrorException('خطا در ارسال پیامک');
                }
                if (code == 34 || code == 30 || code == 38) {
                    throw new common_1.BadRequestException('استعلام با خطا مواجه شد');
                }
                if (code == 7) {
                    throw new common_1.GatewayTimeoutException(' زمان درخواست منقضی شده است, مجدداً تلاش کنید');
                }
            }
            this.message = msg['message'];
        }
    }
}
exports.SabinException = SabinException;
//# sourceMappingURL=sabin.exception.js.map