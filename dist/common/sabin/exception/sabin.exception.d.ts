export declare class SabinException extends Error {
    constructor(msg: string);
}
