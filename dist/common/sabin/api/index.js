"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.apis = void 0;
exports.apis = {
    CREATE_CUSTOMER: '/api/v1/customer/create',
    CREATE_WALLET: '/api/v1/Wallet/CreateWallet',
    GET_CUSTOMER_INFO: '/api/v1/Customer/GetInfoCustomer',
    ISSUE_CARD: '/api/v1/Card/IssueCard',
    CARD_BALANCE: '/api/v1/Card/GetCardBalance',
    WALLET_BALANCE: '/api/v1/wallet/GetWalletBalance',
    SEND_OTP: '/api/v1/card/SendOtpCode',
    SEND_TRANSFER_OTP: '/api/v1/general/SendTransferOtp',
    CHANGE_PIN: '/api/v1/card/changePin',
    CHARGE_CARD: '/api/v1/card/ChargeCard',
    ACTIVE_CARD: '/api/v1/card/ActiveCard',
    DE_ACTIVE_CARD: '/api/v1/card/DeActiveCard',
    GET_TRANSACTION: '/api/v1/card/GetTransaction',
    TRANSFER_WALLET_TO_WALLET: '/api/v1/wallet/TransferBetweenWallets',
    TRANSFER_WALLET_TO_IBAN: '/api/v1/wallet/TransferWalletsToIban',
    IN_QUERY_CARD: '/api/v1/cms/InqueryCard',
    GET_INFO_CUSTOMER_BY_NATIONAL_ID: '/api/v1/customer/GetInfoCustomerByNationalId',
    GET_INFO_CUSTOMER_BY_CARD_NUMBER: '/api/v1/customer/GetInfoCustomerByCardNumber',
    UPDATE_CUSTOMER_ADDRESS: '/api/v1/customer/UpdateCustomerAddress',
    BLOCK_MONEY: '/api/v1/money/BlockMoney',
    UNBLOCK_MONEY: '/api/v1/money/UnBlockMoney',
    SHOW_BLOCK_MONEY: '/api/v1/money/ShowBlockMoney',
    CHARGE_BY_REFERENCE: '/api/v1/Wallet/ChargeWalletByRefrenceNumber'
};
//# sourceMappingURL=index.js.map