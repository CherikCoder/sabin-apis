"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SabinService = void 0;
const path_1 = require("path");
const axios_1 = require("axios");
const fs = require("fs");
const rxjs_1 = require("rxjs");
const crypto = require("crypto");
const RSAXML = require('rsa-xml');
const NodeRSA = require('node-rsa');
class SabinService {
    initialize(client) {
        const privateKeyPath = (0, path_1.join)(__dirname, '../../../private.pem');
        const publicKeyPath = (0, path_1.join)(__dirname, '../../../public.pem');
        if (!fs.existsSync(privateKeyPath))
            throw new Error('Private Key file not found!');
        if (!fs.existsSync(publicKeyPath))
            throw new Error('Public Key file not found!');
        this.privateKey = fs.readFileSync(privateKeyPath, { encoding: 'utf-8' });
        this.publicKey = fs.readFileSync(publicKeyPath, { encoding: 'utf-8' });
        this.encryptionService =
            client.getService('Encryption');
        this.request = axios_1.default.create({
            baseURL: process.env.SABIN_BASE_URL,
            timeout: 180000,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'text/html',
                orgId: process.env.SABIN_ORG_ID,
                Authorization: this.getAuthorizationHeader(),
            },
        });
        this.request.interceptors.request.use(async (config) => {
            let body = config.data;
            if (body && body.hasOwnProperty('CredentialData')) {
                const encryptData = await (0, rxjs_1.firstValueFrom)(this.encryptionService.Encrypt({
                    key: this.publicKey,
                    plainText: body.CredentialData,
                }));
                config.headers.set({
                    envelope: encryptData.envelope,
                    envelope_iv: encryptData.envelopeIV,
                });
                body = {
                    ...body,
                    CredentialData: encryptData.credentialData,
                };
            }
            config.data = JSON.stringify(body);
            config.headers.set({
                signature: this.makeSignature(config.data),
            });
            return config;
        }, (error) => {
            return Promise.reject(error);
        });
        this.request.interceptors.response.use((response) => {
            const { data } = response.data;
            if (data && data.hasOwnProperty('credentialData')) {
                const { envelope, envelope_iv } = response.headers;
                response.data.data.credentialData = this.decryptData({
                    envelope,
                    iv: envelope_iv,
                    credentialData: data.credentialData,
                });
            }
            return response;
        });
    }
    getAuthorizationHeader() {
        const username = process.env.SABIN_USER;
        const password = process.env.SABIN_PASSWORD;
        const encodedCredentials = btoa(`${username}:${password}`);
        return 'Basic ' + encodedCredentials;
    }
    makeSignature(payload) {
        let jsonPayload = payload;
        jsonPayload = jsonPayload.replace(/\n/g, '');
        jsonPayload = jsonPayload.replace(/\r/g, '');
        jsonPayload = jsonPayload.replace(/"/g, '');
        jsonPayload = jsonPayload.replace(/'/g, '');
        jsonPayload = jsonPayload.replace(/ /g, '');
        const message = Buffer.from(jsonPayload, 'utf-8');
        const pemKey = new RSAXML().exportPemKey(this.privateKey);
        const rsa = new NodeRSA(pemKey, 'private');
        return rsa.sign(message, 'hex');
    }
    decryptData({ envelope, iv, credentialData }) {
        const rsa = new RSAXML();
        rsa.importKey(this.privateKey);
        const decryptedData = rsa.decrypt(Buffer.from(envelope, 'hex'));
        if (decryptedData.length !== 26)
            throw new Error('Invalid Envelope Data');
        const key = decryptedData.slice(0, 16);
        const message = Buffer.from(credentialData, 'hex');
        const ivData = Buffer.from(iv, 'hex');
        const decipher = crypto.createDecipheriv('aes-128-cbc', key, ivData);
        const decrypted = Buffer.concat([
            decipher.update(message),
            decipher.final(),
        ]);
        return decrypted.toString();
    }
}
exports.SabinService = SabinService;
//# sourceMappingURL=sabin.service.js.map