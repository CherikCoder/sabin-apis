import { NestMiddleware } from '@nestjs/common';
import * as express from 'express';
export declare class XmlCheckMiddleware implements NestMiddleware {
    use(req: express.Request, res: express.Response, next: express.NextFunction): void;
}
