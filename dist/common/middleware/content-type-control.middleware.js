"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContentTypeControlMiddleware = void 0;
const common_1 = require("@nestjs/common");
let ContentTypeControlMiddleware = class ContentTypeControlMiddleware {
    use(req, res, next) {
        const { method, baseUrl, headers } = req;
        const { authorization } = req.headers;
        if (!authorization)
            return next();
        if (!['GET', 'POST', 'PATCH', 'DELETE', 'PUT'].includes(method.toUpperCase())) {
            throw new common_1.MethodNotAllowedException("متد ارسال شده صحیح نمی باشد");
        }
        if (["GET", "DELETE"].includes(method.toUpperCase()))
            return next();
        const skipPaths = [
            'user/callback',
        ];
        if (skipPaths.find(path => baseUrl.includes(path)))
            return next();
        if (!headers["content-type"]) {
            throw new common_1.BadRequestException('نوع محتوای ارسال شده را تعیین کنید');
        }
        if (!headers["content-type"].includes("application/json"))
            throw new common_1.UnsupportedMediaTypeException("نوع محتوای ارسال شده معتبر نمی باشد");
        next();
    }
};
exports.ContentTypeControlMiddleware = ContentTypeControlMiddleware;
exports.ContentTypeControlMiddleware = ContentTypeControlMiddleware = __decorate([
    (0, common_1.Injectable)()
], ContentTypeControlMiddleware);
//# sourceMappingURL=content-type-control.middleware.js.map