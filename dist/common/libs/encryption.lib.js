"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EncryptionLib = void 0;
const crypto = require("crypto");
const dotEnv = require("dotenv");
dotEnv.config();
class EncryptionLib {
    constructor() {
        this.algorithm = 'aes-256-cbc';
        this.secretKey = process.env.APP_SECRET;
    }
    encrypt(data) {
        try {
            const iv = crypto.randomBytes(10);
            const cipher = crypto.createCipher(this.algorithm, Buffer.from(this.secretKey), iv);
            let encrypted = cipher.update(data, 'utf-8', 'hex');
            encrypted += cipher.final('hex');
            return `${iv.toString('hex')}:${encrypted}`;
        }
        catch (e) {
            console.log(e);
            return '';
        }
    }
    decrypt(data) {
        try {
            const [ivString, encryptedText] = data.split(':');
            const iv = Buffer.from(ivString, 'hex');
            const decipher = crypto.createDecipher(this.algorithm, Buffer.from(this.secretKey), iv);
            let decrypted = decipher.update(encryptedText, 'hex', 'utf-8');
            decrypted += decipher.final('utf-8');
            return decrypted;
        }
        catch (e) {
            console.log(e);
            return '';
        }
    }
}
exports.EncryptionLib = EncryptionLib;
//# sourceMappingURL=encryption.lib.js.map