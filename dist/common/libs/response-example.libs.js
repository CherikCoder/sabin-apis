"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuccessResponse = exports.UnsupportedMediaTypeExceptionResponse = exports.ForbiddenExceptionResponse = exports.NotFoundExceptionResponse = exports.BadRequestExceptionResponse = exports.UnAuthorizedExceptionResponse = exports.InternalExceptionResponse = void 0;
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
class ExceptionResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: false,
        type: Boolean,
    }),
    __metadata("design:type", Boolean)
], ExceptionResponse.prototype, "success", void 0);
class InternalExceptionResponse extends ExceptionResponse {
}
exports.InternalExceptionResponse = InternalExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "خطای داخلی",
        type: String,
    }),
    __metadata("design:type", String)
], InternalExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
        type: Number,
    }),
    __metadata("design:type", Number)
], InternalExceptionResponse.prototype, "statusCode", void 0);
class UnAuthorizedExceptionResponse extends ExceptionResponse {
}
exports.UnAuthorizedExceptionResponse = UnAuthorizedExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "خطای دسترسی - احراز هویت",
        type: String,
    }),
    __metadata("design:type", String)
], UnAuthorizedExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.UNAUTHORIZED,
        type: Number,
    }),
    __metadata("design:type", Number)
], UnAuthorizedExceptionResponse.prototype, "statusCode", void 0);
class BadRequestExceptionResponse extends ExceptionResponse {
}
exports.BadRequestExceptionResponse = BadRequestExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "اطلاعات ارسال شده معتبر نمی باشد",
        type: String,
    }),
    __metadata("design:type", String)
], BadRequestExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.BAD_REQUEST,
        type: Number,
    }),
    __metadata("design:type", Number)
], BadRequestExceptionResponse.prototype, "statusCode", void 0);
class NotFoundExceptionResponse extends ExceptionResponse {
}
exports.NotFoundExceptionResponse = NotFoundExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: 'منبع درخواستی یافت نشد',
        type: String,
    }),
    __metadata("design:type", String)
], NotFoundExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.NOT_FOUND,
        type: Number,
    }),
    __metadata("design:type", Number)
], NotFoundExceptionResponse.prototype, "statusCode", void 0);
class ForbiddenExceptionResponse extends ExceptionResponse {
}
exports.ForbiddenExceptionResponse = ForbiddenExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: 'اجازه دسترسی به این منبع را ندارید',
        type: String,
    }),
    __metadata("design:type", String)
], ForbiddenExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.FORBIDDEN,
        type: Number,
    }),
    __metadata("design:type", Number)
], ForbiddenExceptionResponse.prototype, "statusCode", void 0);
class UnsupportedMediaTypeExceptionResponse extends ExceptionResponse {
}
exports.UnsupportedMediaTypeExceptionResponse = UnsupportedMediaTypeExceptionResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "نوع محتوای ارسال شده معتبر نمی باشد",
        type: String,
    }),
    __metadata("design:type", String)
], UnsupportedMediaTypeExceptionResponse.prototype, "message", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: common_1.HttpStatus.UNSUPPORTED_MEDIA_TYPE,
        type: Number,
    }),
    __metadata("design:type", Number)
], UnsupportedMediaTypeExceptionResponse.prototype, "statusCode", void 0);
class SuccessResponse {
}
exports.SuccessResponse = SuccessResponse;
__decorate([
    (0, swagger_1.ApiProperty)({
        example: true,
        type: Boolean,
    }),
    __metadata("design:type", Boolean)
], SuccessResponse.prototype, "success", void 0);
__decorate([
    (0, swagger_1.ApiProperty)({
        example: "عملیات با موفقیت انجام شد",
        type: String,
    }),
    __metadata("design:type", String)
], SuccessResponse.prototype, "message", void 0);
//# sourceMappingURL=response-example.libs.js.map