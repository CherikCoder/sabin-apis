export declare class EncryptionLib {
    algorithm: string;
    secretKey: string;
    encrypt(data: string): string;
    decrypt(data: string): string;
}
