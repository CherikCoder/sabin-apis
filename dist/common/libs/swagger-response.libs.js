"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExampleResponse = void 0;
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const response_example_libs_1 = require("./response-example.libs");
class ExceptionResponse {
}
__decorate([
    (0, swagger_1.ApiProperty)({
        example: false,
        type: Boolean,
    }),
    __metadata("design:type", Boolean)
], ExceptionResponse.prototype, "success", void 0);
function ExampleResponse(params) {
    const decorators = [
        !params?.showResponse?.showCreated && (0, swagger_1.ApiOkResponse)({
            status: common_1.HttpStatus.OK,
            description: 'موفق',
            type: params?.type,
            isArray: params?.isArray || false
        }),
        params?.showResponse?.showCreated && (0, swagger_1.ApiCreatedResponse)({
            status: common_1.HttpStatus.CREATED,
            description: 'موفق',
            type: params?.type,
            isArray: params?.isArray || false
        }),
        params?.showResponse?.forbidden && (0, swagger_1.ApiForbiddenResponse)({
            status: common_1.HttpStatus.FORBIDDEN,
            description: 'خطای دسترسی به منبع',
            type: response_example_libs_1.ForbiddenExceptionResponse,
        }),
        (0, swagger_1.ApiBadRequestResponse)({
            status: common_1.HttpStatus.BAD_REQUEST,
            description: 'ناموفق',
            type: response_example_libs_1.BadRequestExceptionResponse
        }),
        (params?.showResponse?.showUnAuthorized === undefined || params?.showResponse?.showUnAuthorized) && (0, swagger_1.ApiUnauthorizedResponse)({
            status: common_1.HttpStatus.UNAUTHORIZED,
            description: 'خطای دسترسی-احراز هویت نشده',
            type: response_example_libs_1.UnAuthorizedExceptionResponse
        }),
        (params?.showResponse?.showUnSupportedMediaType === undefined || params?.showResponse?.showUnSupportedMediaType) && (0, swagger_1.ApiUnsupportedMediaTypeResponse)({
            status: common_1.HttpStatus.UNSUPPORTED_MEDIA_TYPE,
            description: 'خطای نوع محتوای ارسال شده',
            type: response_example_libs_1.UnsupportedMediaTypeExceptionResponse
        }),
        (params?.showResponse?.showNotFound === undefined || params?.showResponse?.showNotFound) && (0, swagger_1.ApiNotFoundResponse)({
            status: common_1.HttpStatus.NOT_FOUND,
            description: 'خطای دریافت',
            type: response_example_libs_1.NotFoundExceptionResponse,
        }),
        (0, swagger_1.ApiInternalServerErrorResponse)({
            status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
            description: 'خطای سرور',
            type: response_example_libs_1.InternalExceptionResponse
        }),
    ];
    return (0, common_1.applyDecorators)(...decorators.filter(x => x));
}
exports.ExampleResponse = ExampleResponse;
//# sourceMappingURL=swagger-response.libs.js.map