export declare abstract class Transform<T> {
    abstract transform(item: T): any;
    transformCollection<R>(items: T[]): R[];
}
