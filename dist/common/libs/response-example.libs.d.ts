import { HttpStatus } from "@nestjs/common";
declare class ExceptionResponse {
    success: boolean;
}
export declare class InternalExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class UnAuthorizedExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class BadRequestExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class NotFoundExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class ForbiddenExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class UnsupportedMediaTypeExceptionResponse extends ExceptionResponse {
    message: string;
    statusCode: HttpStatus;
}
export declare class SuccessResponse {
    success?: boolean;
    message?: string;
}
export {};
