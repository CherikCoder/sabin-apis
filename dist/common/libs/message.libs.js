"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessage = void 0;
function getMessage(type, field) {
    switch (type) {
        case 'empty':
            return `وارد کردن ${field} اجباری می‌باشد`;
        case 'string':
            return `${field} باید شامل حروف باشد`;
        case 'number':
            return `${field} فقط می‌تواند عدد باشد`;
    }
}
exports.getMessage = getMessage;
//# sourceMappingURL=message.libs.js.map