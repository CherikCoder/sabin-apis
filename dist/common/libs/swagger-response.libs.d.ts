import { HttpStatus } from '@nestjs/common';
export interface IShowResponseParams {
    showNotFound?: boolean;
    showCreated?: boolean;
    showUnAuthorized?: boolean;
    showUnSupportedMediaType?: boolean;
    forbidden?: boolean;
}
export interface ICreateExampleResponseParams {
    type?: any;
    status?: HttpStatus;
    isGetMethod?: boolean;
    isArray?: boolean;
    showResponse?: IShowResponseParams;
}
export declare function ExampleResponse(params?: ICreateExampleResponseParams): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
