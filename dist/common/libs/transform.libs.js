"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transform = void 0;
class Transform {
    transformCollection(items) {
        return items.map((item) => this.transform(item));
    }
}
exports.Transform = Transform;
//# sourceMappingURL=transform.libs.js.map