import { IActiveCard, IBlockMoney, IChargeCard, ICreateCustomer, ICreateWallet, IGetTransaction, IIssueCard, IResponseApi, ITransferOtp, ITransferWalletToIban, ITransferWalletToWallet, IUnblockMoney, IUpdateCustomerAddress, SabinApi } from "./common";
import { PinCodeDto } from "./common/dto/pin-card.dto";
import { SecondPinDto } from "./common/dto/second-pin.dto";
export declare class AppService {
    private readonly sabinApi;
    constructor(sabinApi: SabinApi);
    createCustomer(args: ICreateCustomer): Promise<IResponseApi>;
    issueCard(args: IIssueCard): Promise<IResponseApi>;
    chargeCard(args: IChargeCard): Promise<IResponseApi>;
    activeCard(args: IActiveCard): Promise<IResponseApi>;
    deActiveCard(args: IActiveCard): Promise<IResponseApi>;
    sendOtp(cardNumber: string): Promise<IResponseApi>;
    cardBalance(cardNumber: string): Promise<IResponseApi>;
    walletBalance(walletNumber: string): Promise<IResponseApi>;
    changeSecondPin(childId: number, newPin: SecondPinDto): Promise<string>;
    changeFirstPin(childId: number, newPinDto: PinCodeDto): Promise<string>;
    createWallet(args: ICreateWallet): Promise<IResponseApi>;
    sendTransferOtp(args: ITransferOtp): Promise<IResponseApi>;
    getTransaction(args: IGetTransaction): Promise<IResponseApi>;
    transferWalletToWallet(args: ITransferWalletToWallet): Promise<IResponseApi>;
    transferWalletToIban(args: ITransferWalletToIban): Promise<IResponseApi>;
    inQueryCard(NationalCode: string): Promise<IResponseApi>;
    getInfoCustomerByNationalId(nationalId: string): Promise<any>;
    getInfoCustomerByCardNumber(wallerOrCard: string): Promise<IResponseApi>;
    updateCustomerAddress(args: IUpdateCustomerAddress): Promise<IResponseApi>;
    blockMoney(args: IBlockMoney): Promise<IResponseApi>;
    unBlockMoney(args: IUnblockMoney): Promise<IResponseApi>;
    showBlockMoney(cardNumber: string): Promise<IResponseApi>;
}
