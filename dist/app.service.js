"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const common_2 = require("./common");
const exception_1 = require("./common/sabin/exception");
let AppService = class AppService {
    constructor(sabinApi) {
        this.sabinApi = sabinApi;
    }
    async createCustomer(args) {
        const response = await this.sabinApi.createCustomer({
            ...args
        });
        return response.data;
    }
    async issueCard(args) {
        const response = await this.sabinApi.issueCard({ ...args });
        return response.data;
    }
    async chargeCard(args) {
        const response = await this.sabinApi.chargeCard({ ...args });
        return response.data;
    }
    async activeCard(args) {
        return this.sabinApi.activeCard(args);
    }
    async deActiveCard(args) {
        return this.sabinApi.deActiveCard(args);
    }
    async sendOtp(cardNumber) {
        const response = await this.sabinApi.sendOtp(cardNumber);
        return response.data;
    }
    async cardBalance(cardNumber) {
        const response = await this.sabinApi.cardBalance(cardNumber);
        return response.data;
    }
    async walletBalance(walletNumber) {
        const response = await this.sabinApi.walletBalance(walletNumber);
        return response.data;
    }
    async changeSecondPin(childId, newPin) {
        try {
            await this.sabinApi.changePin({
                kindPin: common_2.KindPin.INTERNET_PIN,
                newPin: newPin.pin,
                nationalCode: newPin.nationalCode,
                cardNumber: newPin.cardNumber
            });
            return "رمز اول کارت با موفقیت تغییر کرد!";
        }
        catch (e) {
            throw e;
        }
    }
    async changeFirstPin(childId, newPinDto) {
        try {
            await this.sabinApi.changePin({
                kindPin: common_2.KindPin.FIRST_PIN,
                newPin: newPinDto.pin,
                nationalCode: newPinDto.nationalCode,
                cardNumber: newPinDto.cardNumber
            });
            return "رمز اول کارت با موفقیت تغییر کرد!";
        }
        catch (e) {
            throw e;
        }
    }
    async createWallet(args) {
        const response = await this.sabinApi.createWallet({ ...args });
        return response.data;
    }
    async sendTransferOtp(args) {
        try {
            const response = await this.sabinApi.sendTransferOtp({ ...args });
            if (!response.data.isSuccess) {
                throw response.data;
            }
            return response.data;
        }
        catch (e) {
            console.log(e);
            throw new exception_1.SabinException(e);
        }
    }
    async getTransaction(args) {
        const response = await this.sabinApi.getTransaction({ ...args });
        return response.data;
    }
    async transferWalletToWallet(args) {
        const response = await this.sabinApi.transferWalletToWallet({ ...args });
        return response.data;
    }
    async transferWalletToIban(args) {
        const response = await this.sabinApi.transferWalletToIban({ ...args });
        return response.data;
    }
    async inQueryCard(NationalCode) {
        const response = await this.sabinApi.inQueryCard(NationalCode);
        return response.data;
    }
    async getInfoCustomerByNationalId(nationalId) {
        const response = await this.sabinApi.getInfoCustomerByNationalId(nationalId);
        return response.data;
    }
    async getInfoCustomerByCardNumber(wallerOrCard) {
        const response = await this.sabinApi.getInfoCustomerByCardNumber(wallerOrCard);
        return response.data;
    }
    async updateCustomerAddress(args) {
        const response = await this.sabinApi.updateCustomerAddress({ ...args });
        return response.data;
    }
    async blockMoney(args) {
        const response = await this.sabinApi.blockMoney({ ...args });
        return response.data;
    }
    async unBlockMoney(args) {
        const response = await this.sabinApi.unBlockMoney({ ...args });
        return response.data;
    }
    async showBlockMoney(cardNumber) {
        const response = await this.sabinApi.showBlockMoney(cardNumber);
        return response.data;
    }
};
exports.AppService = AppService;
exports.AppService = AppService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [common_2.SabinApi])
], AppService);
//# sourceMappingURL=app.service.js.map