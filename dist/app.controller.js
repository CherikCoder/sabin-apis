"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@nestjs/common");
const app_service_1 = require("./app.service");
const dto_1 = require("./common/dto");
const second_pin_dto_1 = require("./common/dto/second-pin.dto");
const pin_card_dto_1 = require("./common/dto/pin-card.dto");
let AppController = class AppController {
    constructor(appService) {
        this.appService = appService;
    }
    async createCustomer(createCustomerDto) {
        return {
            data: await this.appService.createCustomer(createCustomerDto)
        };
    }
    async createWallet(createWalletDto) {
        return {
            data: await this.appService.createWallet(createWalletDto)
        };
    }
    async issueCard(issueCardDto) {
        return {
            data: await this.appService.issueCard(issueCardDto)
        };
    }
    async inQueryCard(inQueryCardDto) {
        return {
            data: await this.appService.inQueryCard(inQueryCardDto.nationalCode)
        };
    }
    async cardBalance(cardBalanceDto) {
        return {
            data: await this.appService.cardBalance(cardBalanceDto.cardNumber)
        };
    }
    async walletBalance(walletBalanceDto) {
        return {
            data: await this.appService.walletBalance(walletBalanceDto.walletNumber)
        };
    }
    async sendOtp(sendOtpDto) {
        return {
            data: await this.appService.sendOtp(sendOtpDto.cardNumber)
        };
    }
    async sendTransferOtp(sendTransferOtpDto) {
        return {
            data: await this.appService.sendTransferOtp(sendTransferOtpDto)
        };
    }
    async changePin(childId, newPinDto) {
        return {
            data: await this.appService.changeSecondPin(childId, newPinDto)
        };
    }
    async changeFirstPin(childId, newPinDto) {
        return {
            data: await this.appService.changeFirstPin(childId, newPinDto)
        };
    }
    async chargeCard(chargeCardDto) {
        return {
            data: await this.appService.chargeCard(chargeCardDto)
        };
    }
    async activeCard(activeCardDto) {
        return {
            data: await this.appService.activeCard(activeCardDto)
        };
    }
    async deactivateCard(deactivateCardDto) {
        return {
            data: await this.appService.deActiveCard(deactivateCardDto)
        };
    }
    async getTransactions(getTransactionsDto) {
        return {
            data: await this.appService.getTransaction(getTransactionsDto)
        };
    }
    async walletToWallet(walletToWalletDto) {
        return {
            data: await this.appService.transferWalletToWallet(walletToWalletDto)
        };
    }
    async walletToIban(walletToIbanDto) {
        return {
            data: await this.appService.transferWalletToIban(walletToIbanDto)
        };
    }
    async getInfoByNationalCode(infoByNationalCode) {
        return {
            data: await this.appService.getInfoCustomerByNationalId(infoByNationalCode.nationalCode)
        };
    }
    async getInfoByCardNumber(infoByCardDto) {
        return {
            data: await this.appService.getInfoCustomerByCardNumber(infoByCardDto.cardOrWallet)
        };
    }
    async updateCustomerAddress(updateCustomerAddressDto) {
        return {
            data: await this.appService.updateCustomerAddress(updateCustomerAddressDto)
        };
    }
    async blockMoney(blockMoneyDto) {
        return {
            data: await this.appService.blockMoney(blockMoneyDto)
        };
    }
    async showBlockMoney(showBlockMoneyDto) {
        return {
            data: await this.appService.showBlockMoney(showBlockMoneyDto.cardNumber)
        };
    }
    async unblockMoney(unblockMoneyDto) {
        return {
            data: await this.appService.unBlockMoney(unblockMoneyDto)
        };
    }
};
exports.AppController = AppController;
__decorate([
    (0, common_1.Post)("/createCustomer"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateCustomerDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "createCustomer", null);
__decorate([
    (0, common_1.Post)("/createWallet"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateWalletDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "createWallet", null);
__decorate([
    (0, common_1.Post)("/issueCard"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.IssueCardDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "issueCard", null);
__decorate([
    (0, common_1.Post)("/inQueryCard"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.InQueryCardDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "inQueryCard", null);
__decorate([
    (0, common_1.Post)("/cardBalance"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CardBalanceDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "cardBalance", null);
__decorate([
    (0, common_1.Post)("/walletBalance"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.WalletBalanceDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "walletBalance", null);
__decorate([
    (0, common_1.Post)("/sendOtp"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.SendOtpDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "sendOtp", null);
__decorate([
    (0, common_1.Post)("/sendTransferOtp"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.SendTransferOtpDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "sendTransferOtp", null);
__decorate([
    (0, common_1.Post)("/pinCard/change"),
    __param(0, (0, common_1.Query)("childId", common_1.ParseIntPipe)),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, second_pin_dto_1.SecondPinDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "changePin", null);
__decorate([
    (0, common_1.Post)("/firstPin/change"),
    __param(0, (0, common_1.Query)("childId", common_1.ParseIntPipe)),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, pin_card_dto_1.PinCodeDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "changeFirstPin", null);
__decorate([
    (0, common_1.Post)("/chargeCard"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.ChargeCardDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "chargeCard", null);
__decorate([
    (0, common_1.Post)("/activeCard"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.ActiveCardDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "activeCard", null);
__decorate([
    (0, common_1.Post)("/deactivateCard"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.ActiveCardDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "deactivateCard", null);
__decorate([
    (0, common_1.Post)("/getTransactions"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.GetTransactionsDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getTransactions", null);
__decorate([
    (0, common_1.Post)("/transfer/walletToWallet"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.WalletToWalletDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "walletToWallet", null);
__decorate([
    (0, common_1.Post)("/transfer/walletToIban"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.WalletToIbanDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "walletToIban", null);
__decorate([
    (0, common_1.Post)("/getInfo/byNationalCode"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.InfoByNationalCodeDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getInfoByNationalCode", null);
__decorate([
    (0, common_1.Post)("/getInfo/byCardNumber"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.InfoByCardNumberDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "getInfoByCardNumber", null);
__decorate([
    (0, common_1.Post)("/updateCustomerAddress"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.UpdateCustomerAddressDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "updateCustomerAddress", null);
__decorate([
    (0, common_1.Post)("/blockMoney"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.BlockMoneyDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "blockMoney", null);
__decorate([
    (0, common_1.Post)("/showBlockMoney"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.ShowBlockMoneyDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "showBlockMoney", null);
__decorate([
    (0, common_1.Post)("/unblockMoney"),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.UnblockMoneyDto]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "unblockMoney", null);
exports.AppController = AppController = __decorate([
    (0, common_1.Controller)(),
    __metadata("design:paramtypes", [app_service_1.AppService])
], AppController);
//# sourceMappingURL=app.controller.js.map