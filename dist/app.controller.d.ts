import { AppService } from "./app.service";
import { ActiveCardDto, BlockMoneyDto, CardBalanceDto, ChargeCardDto, CreateCustomerDto, CreateWalletDto, GetTransactionsDto, InfoByCardNumberDto, InfoByNationalCodeDto, InQueryCardDto, IssueCardDto, SendOtpDto, SendTransferOtpDto, ShowBlockMoneyDto, UnblockMoneyDto, UpdateCustomerAddressDto, WalletBalanceDto, WalletToIbanDto, WalletToWalletDto } from "./common/dto";
import { SecondPinDto } from "./common/dto/second-pin.dto";
import { PinCodeDto } from "./common/dto/pin-card.dto";
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    createCustomer(createCustomerDto: CreateCustomerDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    createWallet(createWalletDto: CreateWalletDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    issueCard(issueCardDto: IssueCardDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    inQueryCard(inQueryCardDto: InQueryCardDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    cardBalance(cardBalanceDto: CardBalanceDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    walletBalance(walletBalanceDto: WalletBalanceDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    sendOtp(sendOtpDto: SendOtpDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    sendTransferOtp(sendTransferOtpDto: SendTransferOtpDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    changePin(childId: number, newPinDto: SecondPinDto): Promise<{
        data: string;
    }>;
    changeFirstPin(childId: number, newPinDto: PinCodeDto): Promise<{
        data: string;
    }>;
    chargeCard(chargeCardDto: ChargeCardDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    activeCard(activeCardDto: ActiveCardDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    deactivateCard(deactivateCardDto: ActiveCardDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    getTransactions(getTransactionsDto: GetTransactionsDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    walletToWallet(walletToWalletDto: WalletToWalletDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    walletToIban(walletToIbanDto: WalletToIbanDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    getInfoByNationalCode(infoByNationalCode: InfoByNationalCodeDto): Promise<{
        data: any;
    }>;
    getInfoByCardNumber(infoByCardDto: InfoByCardNumberDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    updateCustomerAddress(updateCustomerAddressDto: UpdateCustomerAddressDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    blockMoney(blockMoneyDto: BlockMoneyDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    showBlockMoney(showBlockMoneyDto: ShowBlockMoneyDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
    unblockMoney(unblockMoneyDto: UnblockMoneyDto): Promise<{
        data: import("./common").IResponseApi;
    }>;
}
